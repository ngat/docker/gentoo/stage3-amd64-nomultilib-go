#!/usr/bin/env sh

echo " -> Get latest image"
docker pull registry.gitlab.com/ngat/docker/gentoo/stage3-amd64-nomultilib:latest

echo " -> Create temporary container"
docker create -i --cap-add ALL registry.gitlab.com/ngat/docker/gentoo/stage3-amd64-nomultilib:latest /bin/bash

echo " -> Getting Temporary Container"
TMP_CONTAINER=`docker ps -a -l -q`

echo " -> Starting temporary container"
docker start $TMP_CONTAINER

echo " -> Copying package.accept_keywords files to /etc/portage"
docker cp package.accept_keywords $TMP_CONTAINER:/etc/portage/package.accept_keywords/go

echo " -> Copying build.sh file to /build.sh"
docker cp build.sh $TMP_CONTAINER:/build.sh

echo " -> Running build.sh script"
docker exec -i $TMP_CONTAINER /bin/bash /build.sh

echo " -> Stopping temporary container"
docker stop $TMP_CONTAINER

DATE=$(date +%Y%m%d)
DOCKER_TAG="registry.gitlab.com/ngat/docker/gentoo/stage3-amd64-nomultilib-go"
echo " -> Committing Container to Image"
docker commit -c "CMD /sbin/init" $TMP_CONTAINER $DOCKER_TAG:latest
docker push $DOCKER_TAG:latest

echo " -> Attaching Tags and pushing to registry.gitlab.com"
NEW_IMAGE=`docker images -q | head -n1`

echo " -> Adding $DOCKER_TAG to $NEW_IMAGE"
docker tag $NEW_IMAGE $DOCKER_TAG:$DATE
docker push $DOCKER_TAG:$DATE

echo " -> Remove temporary container"
docker rm $TMP_CONTAINER

echo " -> Remove temporary image"
docker rmi $DOCKER_TAG:$DATE
docker rmi $DOCKER_TAG:latest
